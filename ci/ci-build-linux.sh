#!/bin/bash

set -ex

source ci/linux-common.sh

# Entering the git repositiory
pushd /opt/build/media-git

# If the remote is not present, add and fetch it
if [ -z "${TEST_REMOTE}" ]; then
	TEST_REMOTE="test"
	git remote add "test" "${GIT_REPO}"
fi

# Fetch the latest version of the branch
git fetch --depth=1 ${TEST_REMOTE} ${GIT_BRANCH}
git checkout ${TEST_REMOTE}/${GIT_BRANCH}

# Apply the patchset if needed
if [ ! -z "${TEST_PATCHSET_ID}" ]; then
	echo "Getting patchset from Message-id: ${TEST_PATCHSET_ID}"
	git config --add user.name "${GITLAB_USER_NAME}"
	git config --add user.email "${GITLAB_USER_EMAIL}"
	b4 shazam "${TEST_PATCHSET_ID}"
fi

# Leaving the git repository
popd

# Compress the source tree to use as artifact if the remote is not in the docker image
if [ "${TEST_REMOTE}" == "test" ]; then
	tar --zstd --exclude-vcs --exclude-vcs-ignore -cf media-git.tar.zstd /opt/build/media-git
fi

cp /opt/env.sh ./env.sh

echo "--- env.sh: ---"
cat env.sh
echo "---------------"

./build.sh -virtme -build-only none

tar --zstd -cf virtme.tar.zstd \
	/opt/build/trees/virtme/media-git/arch/x86/boot/bzImage \
	/opt/build/trees/virtme/media-git/virtme-modules \
	/opt/build/trees/virtme/media-git/modules.* \
	/opt/build/trees/virtme/media-git/.config \
	/opt/build/trees/virtme/media-git/System.map

